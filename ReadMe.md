# Lights Out

The following is a project created with ReactJs for the frontend and ASP.Net Core API to serve as an api to create new games and make moves on the board. When creating a game an id is given and kept track of as to make the following move. When making a move the board is checked for lights as to check whether the user won or not.


## Instructions

- Run the migrations to create the games table (currently using mssqllocaldb)
- Host the API via one of the following
  - IIS
  - IIS Express
  - Debug
- Change api_url in frontend -> config/config.json
- Start frontend via npm start