import React from 'react';
import Grid from '../components/Grid';

import classess from './LightsOut/lightsOut.module.css'

function LightsOut() {
	return (
		<div>
			<h1 className={classess.Heading}>Lights Out</h1>
			<Grid />
		</div>
	);
}

export default LightsOut;
