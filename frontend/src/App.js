import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import LightsOut from './pages/LightsOut';

function App() {
	return (
		<Router>
			<div>
				<Route exact path="/" component={LightsOut} />
			</div>
		</Router>
	);
}

export default App;
