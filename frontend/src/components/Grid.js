import React from 'react';
import axios from 'axios';

import config from '../config/config.json';
import classes from './Grid/grid.module.css';

class Grid extends React.Component {
	state = {
		grid: [],
		gameID: 0
	};

	componentDidMount() {
		axios.post(config.api_url + "/api/LightsOut/Create", {
			GridSize: 5,
			LightsOn: 2
		}).then(result => {
			console.log(result);
			this.setState({ grid: result.data.grid, gameID: result.data.gameID });
		});
	}

	handleClick(row, column) {
		axios.post(config.api_url + "/api/LightsOut/MakeMove", {
			GameID: this.state.gameID,
			MoveRow: row,
			MoveColumn: column
		}).then(result => {
			this.setState({ grid: result.data.updatedGrid });

			if(result.data.won)
			{
				alert("Won");
			}
		});
	}

	render() {
		return (
			<div className={classes.Grid}>
				{this.state.grid.map((row, rowIndex) => (
					<div key={rowIndex} className={classes.GridRow}>
						{row.map((column, columnIndex) => (
						<div key={columnIndex} className={`${classes.GridColumn}`} onClick={() => this.handleClick(rowIndex, columnIndex)}>
							<div className={`${classes.GridContent} ${(column === true ? classes.Active : '')}`}>
								
							</div>
						</div>
						))}
					</div>
				))}
			</div>
		);
	}
}

export default Grid;