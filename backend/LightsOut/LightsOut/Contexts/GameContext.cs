﻿using LightsOut.Models;
using Microsoft.EntityFrameworkCore;

namespace LightsOut.Contexts
{
    public class GameContext : DbContext
    {
        public GameContext(DbContextOptions<GameContext> options) : base(options)
        {
        }

        public DbSet<Game> Games { get; set; }
    }
}
