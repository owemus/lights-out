﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Contracts
{
    public class MakeMoveRequest
    {
        public int GameID { get; set; }
        public int MoveRow { get; set; }
        public int MoveColumn { get; set; }
    }
}
