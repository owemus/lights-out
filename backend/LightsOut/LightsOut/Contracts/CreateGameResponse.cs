﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Contracts
{
    public class CreateGameResponse
    {
        public bool Success { get; set; }
        public int GameID { get; set; }
        public List<List<bool>> Grid { get; set; }
    }
}
