﻿using LightsOut.Models;

namespace LightsOut.Contracts
{
    public interface IGameService
    {
        Game Add(Game game);
        Game GetById(int id);
        bool Update(int id, Game game);
    }
}
