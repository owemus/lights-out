﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Contracts
{
    public class MakeMoveResponse
    {
        public bool Success { get; set; }
        public List<List<bool>> UpdatedGrid { get; set; }
        public bool Won { get; set; }
    }
}
