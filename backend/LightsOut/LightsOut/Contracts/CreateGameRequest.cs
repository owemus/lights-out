﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Contracts
{
    public class CreateGameRequest
    {
        public int GridSize { get; set; }
        public int LightsOn { get; set; }
    }
}
