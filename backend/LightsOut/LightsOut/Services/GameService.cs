﻿using LightsOut.Models;
using System;
using Microsoft.Extensions.DependencyInjection;
using LightsOut.Contexts;
using LightsOut.Contracts;

namespace LightsOut.Services
{
    public class GameService : IGameService
    {
        private readonly GameContext _context;

        public GameService(GameContext context)
        {
            _context = context;
        }

        public Game Add(Game game)
        {
            _context.Games.Add(game);
            _context.SaveChanges();

            return game;
        }

        public Game GetById(int id)
        {
            return _context.Games.Find(id);
        }

        public bool Update(int id, Game game)
        {
            var findGame = _context.Games.Find(id);
            findGame = game;
            _context.SaveChanges();

            return true;
        }
    }
}
