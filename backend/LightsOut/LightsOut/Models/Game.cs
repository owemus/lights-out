﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Models
{
    public class Game
    {
        public int ID { get; set; }
        public string CurrentGrid { get; set; }
        public int Moves { get; set; }
    }
}
