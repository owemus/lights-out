﻿using System;
using System.Collections.Generic;
using LightsOut.Contracts;
using LightsOut.Models;
using LightsOut.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightsOut.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LightsOutController : ControllerBase
    {
        private readonly IGameService _gameService;

        public LightsOutController(IGameService gameService)
        {
            _gameService = gameService;
        }

        // POST: api/LightsOut/Create
        [HttpPost]
        [Route("Create")]
        public CreateGameResponse Create([FromBody] CreateGameRequest createGame)
        {
            List<List<bool>> Grid = new List<List<bool>>();
            Random rnd = new Random();

            for (int i = 0; i < createGame.GridSize; i++)
            {
                List<bool> tempRow = new List<bool>();

                for (int j = 0; j < createGame.GridSize; j++)
                {
                    tempRow.Add(false);
                }

                Grid.Add(tempRow);
            }

            for (int k = 0; k < createGame.LightsOn; k++)
            {
                int rndRow = rnd.Next(createGame.GridSize);
                int rndColumn = rnd.Next(createGame.GridSize);

                Grid[rndRow][rndColumn] = true;
            }

            var createdGame = _gameService.Add(new Game()
            {
                CurrentGrid = JsonConvert.SerializeObject(Grid),
                Moves = 0
            });

            return new CreateGameResponse() {
                Success = true,
                Grid = Grid,
                GameID = createdGame.ID
            };
        }

        [HttpPost]
        [Route("MakeMove")]
        public MakeMoveResponse MakeMove([FromBody] MakeMoveRequest makeMove)
        {
            Game findGame = _gameService.GetById(makeMove.GameID);

            if(findGame != null)
            {
                List<List<bool>> parsedGrid = JsonConvert.DeserializeObject<List<List<bool>>>(findGame.CurrentGrid);

                parsedGrid[makeMove.MoveRow][makeMove.MoveColumn] = !parsedGrid[makeMove.MoveRow][makeMove.MoveColumn];

                if (parsedGrid.Count - 1 > makeMove.MoveRow)
                {
                    parsedGrid[makeMove.MoveRow + 1][makeMove.MoveColumn] = !parsedGrid[makeMove.MoveRow + 1][makeMove.MoveColumn];
                }
                if(makeMove.MoveRow != 0)
                {
                    parsedGrid[makeMove.MoveRow - 1][makeMove.MoveColumn] = !parsedGrid[makeMove.MoveRow - 1][makeMove.MoveColumn];
                }

                if (parsedGrid[makeMove.MoveRow].Count - 1 > makeMove.MoveColumn)
                {
                    parsedGrid[makeMove.MoveRow][makeMove.MoveColumn + 1] = !parsedGrid[makeMove.MoveRow][makeMove.MoveColumn + 1];
                }
                if (makeMove.MoveColumn != 0)
                {
                    parsedGrid[makeMove.MoveRow][makeMove.MoveColumn - 1] = !parsedGrid[makeMove.MoveRow][makeMove.MoveColumn - 1];
                }

                findGame.Moves++;
                findGame.CurrentGrid = JsonConvert.SerializeObject(parsedGrid);
                _gameService.Update(makeMove.GameID, findGame);

                return new MakeMoveResponse()
                {
                    Success = true,
                    UpdatedGrid = parsedGrid,
                    Won = validateGrid(parsedGrid)
                };
            }
            else
            {
                return new MakeMoveResponse()
                {
                    Success = false
                };
            }
        }

        private bool validateGrid(List<List<bool>> grid)
        {
            bool won = true;

            foreach(List<bool> row in grid)
            {
                foreach (bool col in row)
                {
                    if (col)
                    {
                        won = false;
                    }
                }
            }

            return won;
        }
    }
}
