﻿using LightsOut.Contracts;
using LightsOut.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LightsOut.Testing.Mocking
{
    public class GameServiceFaker : IGameService
    {
        public Game Add(Game game)
        {
            return new Game()
            {
                ID = 1,
                Moves = 0,
                CurrentGrid = JsonConvert.SerializeObject(new List<List<bool>>()
                {
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, true, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    }
                })
            };
        }

        public Game GetById(int id)
        {
            return new Game()
            {
                ID = 1,
                Moves = 0,
                CurrentGrid = JsonConvert.SerializeObject(new List<List<bool>>()
                {
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, true, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    },
                    new List<bool> ()
                    {
                        false, false, false, false, false
                    }
                })
            };
        }

        public bool Update(int id, Game game)
        {
            return true;
        }
    }
}
