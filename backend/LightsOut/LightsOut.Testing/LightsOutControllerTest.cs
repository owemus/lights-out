using System;
using Xunit;
using LightsOut.Controllers;
using LightsOut.Services;
using LightsOut.Contracts;
using LightsOut.Testing.Mocking;

namespace LightsOut.Testing
{
    public class LightsOutControllerTest
    {
        LightsOutController _controller;
        IGameService _service;

        public LightsOutControllerTest()
        {
            _service = new GameServiceFaker();
            _controller = new LightsOutController(_service);
        }

        [Fact]
        public void CreateGame()
        {
            var result = _controller.Create(new CreateGameRequest()
            {
                GridSize = 5,
                LightsOn = 2
            });

            Assert.True(result.Success);
            Assert.True(result.GameID == 1);
            Assert.True(result.Grid.Count == 5);
            Assert.True(result.Grid[0].Count == 5);
        }

        [Fact]
        public void MakeMove()
        {
            var result = _controller.MakeMove(new MakeMoveRequest()
            {
                GameID = 1,
                MoveColumn = 1,
                MoveRow = 1
            });

            Assert.True(result.Success);
            Assert.True(!result.Won);
            Assert.True(result.UpdatedGrid.Count == 5);
            Assert.True(result.UpdatedGrid[0].Count == 5);
        }
    }
}
